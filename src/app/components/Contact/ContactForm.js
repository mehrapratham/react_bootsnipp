import React, { Component } from "react";
import { connect } from "react-redux";
import { contactus } from "../../api/auth";
import { IsValidForm, validateField } from "../Common/validation";
import { toastr } from "react-redux-toastr";

class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contact: {
        firstName: "",
        lastName: "",
        email: "",
        message: ""
      },
      errors: [],
      loading: false
    };
  }

  onChange(key, event) {
    let { contact } = this.state;
    contact[key] = event.target.value;
    this.setState({ contact });
  }

  onSubmit() {
    let fields = ["firstName", "lastName", "email", "message"];
    let formValidation = IsValidForm(fields, this.state.contact);
    this.setState({ errors: formValidation.errors });
    if (formValidation.validate) {
      this.setState({ loading: true });
      this.props.dispatch(contactus(this.state.contact)).then(res => {
        toastr.success("SucessFull", res);
        this.setState({ loading: false });
        this.setState({
          contact: { firstName: "", lastName: "", email: "", message: "" }
        });
      });
    }
  }

  showError(key) {
    let errors = this.state.errors;
    if (errors[key] && errors[key].length) {
      return true;
    }
    return false;
  }
  getError(key) {
    let errors = this.state.errors;
    if (errors[key] && errors[key].length) {
      return typeof errors[key] === "object"
        ? errors[key].join(",")
        : errors[key];
    }
    return false;
  }

  onBlur(key, event) {
    let { errors } = this.state;
    let value = event.target.value;
    if (!event.target.value) {
      errors[key] = "";
    } else {
      errors[key] = validateField(key, value);
    }
    this.setState({ errors });
  }
  clearError(key) {
    let errors = this.state.errors;
    errors[key] = "";
    this.setState({ errors });
  }

  render() {
    let { contact } = this.state;
    return (
      <div className="p-5 bg-white border">
        <div className="row form-group">
          <div className="col-md-12 mb-3 mb-md-0">
            <div className="row">
              <div className="col-md-6 mb-3 mb-md-0">
                <label className="font-weight-bold">First Name</label>
                <input
                  type="text"
                  id="fullname"
                  className="form-control"
                  placeholder="Full Name"
                  value={contact.firstName}
                  onChange={this.onChange.bind(this, "firstName")}
                  onBlur={this.onBlur.bind(this, "firstName")}
                  onFocus={this.clearError.bind(this, "firstName")}
                />
                {!!this.showError("firstName") ? (
                  <p className="error-message">{this.getError("firstName")} </p>
                ) : null}
              </div>
              <div className="col-md-6 mb-3 mb-md-0">
                <label className="font-weight-bold">Last Name</label>
                <input
                  type="text"
                  id="fullname"
                  className="form-control"
                  placeholder="Full Name"
                  value={contact.lastName}
                  onChange={this.onChange.bind(this, "lastName")}
                />
                {!!this.showError("lastName") ? (
                  <p className="error-message">{this.getError("lastName")} </p>
                ) : null}
              </div>
            </div>
          </div>
        </div>
        <div className="row form-group">
          <div className="col-md-12">
            <label className="font-weight-bold">Email</label>
            <input
              type="email"
              id="email"
              className="form-control"
              placeholder="Email Address"
              value={contact.email}
              onChange={this.onChange.bind(this, "email")}
            />
            {!!this.showError("email") ? (
              <p className="error-message">{this.getError("email")} </p>
            ) : null}
          </div>
        </div>
        <div className="row form-group">
          <div className="col-md-12">
            <label className="font-weight-bold">Message</label>
            <textarea
              name="message"
              id="message"
              cols="30"
              rows="5"
              className="form-control"
              placeholder="Say hello to us"
              value={contact.message}
              onChange={this.onChange.bind(this, "message")}
            />
            {!!this.showError("message") ? (
              <p className="error-message">{this.getError("message")} </p>
            ) : null}
          </div>
        </div>
        <div className="row form-group">
          <div className="col-md-12">
            <button
              disabled={this.state.loading}
              onClick={this.onSubmit.bind(this)}
              className="btn btn-primary"
              style={{ width: 135 }}
            >
              {this.state.loading ? (
                <i class="fas fa-spinner fa-spin" />
              ) : (
                "Send Message"
              )}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(ContactForm);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
