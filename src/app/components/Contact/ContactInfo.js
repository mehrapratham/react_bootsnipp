import React from "react";

export default props => {
  return (
    <div className="p-4 mb-3 bg-white contactDetail">
      <h3 className="h6 text-black mb-3 text-uppercase">Contact Info</h3>
      <p className="mb-0 font-weight-bold">Address</p>
      <p className="mb-4">
        203 Fake St. Mountain View, San Francisco, California, USA
      </p>
      <p className="mb-0 font-weight-bold">Phone</p>
      <p className="mb-4">
        <a href="#">+1 232 3235 324</a>
      </p>
      <p className="mb-0 font-weight-bold">Email Address</p>
      <p className="mb-0">
        <a href="#">youremail@domain.com</a>
      </p>
    </div>
  );
};
