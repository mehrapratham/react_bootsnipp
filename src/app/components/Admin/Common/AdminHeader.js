import React, { Component } from "react";
import { connect } from "react-redux";
import { Header } from "react-adminlte-dash";

class AdminHeader extends Component {
  logout() {
    localStorage.removeItem("Admin_token");
    this.props.history.push("/admin-login");
  }

  render() {
    return (
      <Header.UserMenu
        image={
          "https://visualpharm.com/assets/30/User-595b40b85ba036ed117da56f.svg"
        }
        profileAction={() => console.log("here")}
        signOutAction={this.logout.bind(this)}
        name="Rupinderpal Singh"
        href="/some/link"
        key="1"
        sidebarMini={true}
        sidebarCollapse={true}
      />
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(AdminHeader);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
