import React, { Component } from "react";
import ProgressiveImage from "react-progressive-image-loading";

export default class DetailCard extends Component {
  render() {
    return (
      <div className="bg-light">
        {/*<div className="row mb-5">
          <div className="col-md-6">
            <strong className="text-success h1 mb-3">$1,000,500</strong>
          </div>
          <div className="col-md-6">
            <ul className="property-specs-wrap mb-3 mb-lg-0  float-lg-right">
              <li>
                <span className="property-specs">Beds</span>
                <span className="property-specs-number">
                  2 <sup>+</sup>
                </span>
              </li>
              <li>
                <span className="property-specs">Baths</span>
                <span className="property-specs-number">2</span>
              </li>
              <li>
                <span className="property-specs">SQ FT</span>
                <span className="property-specs-number">7,000</span>
              </li>
            </ul>
          </div>
        </div>
        <div className="row mb-5">
          <div className="col-md-6 col-lg-4 text-left border-bottom border-top py-3">
            <span className="d-inline-block text-black mb-0 caption-text">
              Home Type
            </span>
            <strong className="d-block">Condo</strong>
          </div>
          <div className="col-md-6 col-lg-4 text-left border-bottom border-top py-3">
            <span className="d-inline-block text-black mb-0 caption-text">
              Year Built
            </span>
            <strong className="d-block">2018</strong>
          </div>
          <div className="col-md-6 col-lg-4 text-left border-bottom border-top py-3">
            <span className="d-inline-block text-black mb-0 caption-text">
              Price/Sqft
            </span>
            <strong className="d-block">$520</strong>
          </div>
        </div>*/}
        <h2 className="h4 text-black">More Info</h2>
        <p>{this.props.detail.description}</p>
      </div>
    );
  }
}
