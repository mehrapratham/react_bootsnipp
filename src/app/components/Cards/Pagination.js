import React, { Component } from "react";
import Pagination from "react-js-pagination";

export default class ReactPagination extends Component {
  render() {
    let { currentPage, onChange, totalPages, perPage } = this.props;
    return (
      <div className="site-pagination">
        <Pagination
          activePage={currentPage}
          itemsCountPerPage={perPage ? perPage : 12}
          totalItemsCount={totalPages}
          pageRangeDisplayed={5}
          onChange={onChange.bind(this)}
        />
      </div>
    );
  }
}
