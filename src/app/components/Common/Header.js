import React from "react";
import { Link } from "react-router-dom";
import { Navbar, Form, Nav, NavDropdown, FormControl } from "react-bootstrap";
import Search from "../Cards/Search";
import { connect } from "react-redux";
import Signup from "../Auth/Signup";
import Login from "../Auth/Login";
import { getAllCategories } from "../../api/listing";
import { getUserScriptsIds } from "../../api/auth";
import { initialUserDataLoad, logout } from "../../actions/auth";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showSignup: false,
      showLogin: false
    };
  }
  componentDidMount() {
    this.props.dispatch(getAllCategories(1, 10));
    this.props.dispatch(initialUserDataLoad());
    this.props.dispatch(getUserScriptsIds());
  }
  OnClickCategory(id) {
    if (id) {
      this.props.history.push(`/?category=${id}`);
    }
  }

  handleClose() {
    this.setState({ showSignup: false, showLogin: false });
  }

  handleOpen() {
    this.setState({ showSignup: true });
  }

  handleLogin() {
    this.setState({ showLogin: true });
  }

  goToUserScript() {
    this.props.history.push(`/myscripts`);
  }

  isLoggeding() {
    let { auth } = this.props;
    if (!auth.userToken) {
      return (
        <React.Fragment>
          <li>
            <a onClick={this.handleOpen.bind(this)}>Signup</a>
          </li>
          <li>
            <a onClick={this.handleLogin.bind(this)}>Login</a>
          </li>
        </React.Fragment>
      );
    } else {
      return (
        <li className="has-children userLogged">
          <a>
            <img src={"/assets/img/userIcon.png"} />
          </a>
          <ul className="dropdown">
            <li>
              <a onClick={this.goToUserScript.bind(this)}>
                My Favorite Scripts
              </a>
            </li>
            {/*<li>
              <a href="#">Profile</a>
            </li>*/}
            <li>
              <a onClick={this.logout.bind(this)}>Logout</a>
            </li>
          </ul>
        </li>
      );
    }
  }

  logout() {
    this.props.dispatch(logout());
    this.props.history.push(`/`);
  }
  render() {
    let { categories } = this.props.listing;
    console.log(this.props);
    return (
      <div className="site-wrap text-left">
        <div className="site-mobile-menu">
          <div className="site-mobile-menu-header">
            <div className="site-mobile-menu-close mt-3">
              <span className="icon-close2 js-menu-toggle" />
            </div>
          </div>
          <div className="site-mobile-menu-body" />
        </div>
        <div className="site-navbar">
          <div className="container py-1">
            <div className="row align-items-center">
              <div className="col-8 col-md-8 col-lg-4">
                <h1 className="">
                  <Link to={"/"} className="h5 text-uppercase text-black">
                    <strong>
                      Graphicskart
                      <span className="text-danger">.com</span>
                    </strong>
                  </Link>
                </h1>
              </div>

              <div className="col-4 col-md-4 col-lg-8">
                <Search history={this.props.history} />
                <nav
                  className="site-navigation text-right text-md-right"
                  role="navigation"
                >
                  <div className="d-inline-block d-lg-none ml-md-0 mr-auto py-3">
                    <a
                      href="#"
                      className="site-menu-toggle js-menu-toggle text-black"
                    >
                      <span className="icon-menu h3" />
                    </a>
                  </div>
                  <ul className="site-menu js-clone-nav d-none d-lg-block">
                    <li>
                      <Link to={"/"}>Home</Link>
                    </li>
                    <li className="has-children">
                      <a>Categories</a>
                      <ul className="dropdown">
                        {categories.map((item, key) => {
                          return (
                            <li
                              key={key}
                              onClick={this.OnClickCategory.bind(
                                this,
                                item.slug
                              )}
                            >
                              <a>{item.title}</a>
                            </li>
                          );
                        })}
                      </ul>
                    </li>
                    {/*<li>
                      <Link to={"/about"}>About</Link>
                    </li>*/}
                    <li>
                      <Link to={"/contact"}>Contact</Link>
                    </li>
                    {this.isLoggeding()}
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <Signup
          isOpen={this.state.showSignup}
          onClose={this.handleClose.bind(this)}
        />
        <Login
          isOpen={this.state.showLogin}
          onClose={this.handleClose.bind(this)}
          history={this.props.history}
        />
      </div>
    );
  }
}
const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Header);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
