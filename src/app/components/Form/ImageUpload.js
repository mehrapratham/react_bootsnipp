import React from "react";
export default class ImageUpload extends React.Component {
  uploadImage(e) {
    let reader = new FileReader();
    let file = e.target.files[0];
    var preview = document.querySelector("#img");
    this.props.onImageUpload(file);

    reader.addEventListener(
      "load",
      function() {
        if (reader.result) {
          preview.src = reader.result;
        }
      },
      false
    );

    if (file) {
      reader.readAsDataURL(file);
    }
  }

  render() {
    return (
      <div className="imageUpload">
        <input type="file" onChange={this.uploadImage.bind(this)} />
        <img scr="" alt="img" id="img" />
      </div>
    );
  }
}
