import React from "react";
import { Form, ControlLabel, FormGroup, FormControl } from "react-bootstrap";
export default class SelectField extends React.Component {
  render() {
    let { valueKey, dataKey } = this.props;
    return (
      <div
        className={this.props.iconClassName ? "selectBox" : "selectBox noIcon"}
      >
        <i className={this.props.iconClassName} />
        <Form.Group className="inputCon">
          <Form.Control as="select">
            <option>{this.props.placeholder}</option>
            {this.props.data &&
              this.props.data.map((item, index) => {
                return (
                  <option key={index} value={item[valueKey]}>
                    {item[dataKey]}
                  </option>
                );
              })}
          </Form.Control>
        </Form.Group>
      </div>
    );
  }
}
