import React from "react";
import { FormControl } from "react-bootstrap";
export default class InputText extends React.Component {
  render() {
    return (
      <div
        className={this.props.iconClassName ? "inputCon" : "inputCon noIcon"}
      >
        <i className={this.props.iconClassName} />
        <FormControl
          type={this.props.type}
          placeholder={this.props.placeholder}
          value={this.props.value}
          onChange={this.props.onChange}
          className={this.props.className}
          onKeyUp={this.props.onKeyUp}
        />
      </div>
    );
  }
}
