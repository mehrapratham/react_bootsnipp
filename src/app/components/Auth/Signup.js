import React, { Component } from "react";
import { connect } from "react-redux";
import { Modal, Button } from "react-bootstrap";
import InputText from "../Form/InputText";
import { signupApi } from "../../api/auth";
import { toastr } from "react-redux-toastr";
import { IsValidForm } from "../Common/validation";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signupData: {
        firstName: "",
        lastName: "",
        email: "",
        password: ""
      },
      loading: false,
      errorMsg: ""
    };
  }

  onChange(key, event) {
    let { signupData } = this.state;
    signupData[key] = event.target.value;
    this.setState({ signupData, errorMsg: "" });
  }

  resetData() {
    let signupData = {
      firstName: "",
      lastName: "",
      email: "",
      password: ""
    };
    this.setState({ signupData });
  }

  signupFunction(e) {
    e.preventDefault();
    let { signupData } = this.state;
    this.setState({ loading: true });
    this.props.dispatch(signupApi(signupData)).then(res => {
      if (!res.errors) {
        this.resetData();
        toastr.success("SucessFull", res);
        this.onClose();
      } else {
        this.setState({ errorMsg: res.errors });
      }
      this.setState({ loading: false });
    });
  }

  onClose() {
    this.setState({ errorMsg: "" });
    this.resetData();
    this.props.onClose();
  }

  renderErrorMsg() {
    let { errorMsg } = this.state;
    if (typeof errorMsg == "string") {
      return errorMsg;
    }
  }

  isDisable() {
    let { loading } = this.state;
    let fields = ["firstName", "lastName", "email", "password"];
    let formValidation = IsValidForm(fields, this.state.signupData);
    if (!formValidation.validate || loading) {
      return true;
    }
  }

  render() {
    let { signupData } = this.state;
    return (
      <Modal
        show={this.props.isOpen}
        onHide={this.onClose.bind(this)}
        className="signup login"
      >
        <Modal.Body>
          <form role="form" onSubmit={this.signupFunction.bind(this)}>
            <div className="loginAvatar">
              G<span>K</span>
            </div>
            <div className="box box-warning">
              <div className="box-body">
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="First Name"
                    value={signupData.firstName}
                    onChange={this.onChange.bind(this, "firstName")}
                  />
                </div>
                <div className="form-group">
                  <InputText
                    type="text"
                    placeholder="Last Name"
                    value={signupData.lastName}
                    onChange={this.onChange.bind(this, "lastName")}
                  />
                </div>
                <div className="form-group">
                  <InputText
                    type="email"
                    placeholder="Email"
                    value={signupData.email}
                    onChange={this.onChange.bind(this, "email")}
                  />
                </div>

                <div className="form-group">
                  <InputText
                    type="password"
                    placeholder="Password"
                    value={signupData.password}
                    onChange={this.onChange.bind(this, "password")}
                  />
                </div>
              </div>
            </div>
            <div className="text-center">
              <p className="errormsg">{this.renderErrorMsg()}</p>
            </div>
            <div className="text-right">
              <button
                type="submit"
                className="btn btn-primary"
                disabled={this.isDisable()}
              >
                Signup
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(state => ({}, mapDispatch))(Signup);

const mapDispatch = dispatch => {
  const allActionProps = Object.assign({}, dispatch);
  return allActionProps;
};
