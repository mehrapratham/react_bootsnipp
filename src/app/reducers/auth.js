import types from "../types";

const initialState = {
  isAuthenticated: false,
  currentUser: {},
  userScriptList: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.AUTHENTICATE:
      return {
        ...state,
        isAuthenticated: action.authenticated
      };

    case types.SAVE_USER_AUTH:
      let userToken;
      if (action.payload && action.payload.token) {
        userToken = action.payload.token;
      } else {
        userToken = null;
      }
      return {
        ...state,
        currentUser: action.payload,
        userToken: userToken
      };
    case types.SAVE_USER_SCRIPT:
      let index = state.userScriptList.indexOf(action.payload);
      if (index == -1) {
        return {
          ...state,
          userScriptList: [...state.userScriptList, action.payload]
        };
      } else {
        return {
          ...state,
          userScriptList: [
            ...state.userScriptList.slice(0, index),
            ...state.userScriptList.slice(index + 1)
          ]
        };
      }
    case types.SAVE_USER_SCRIPT_IDS:
      return {
        ...state,
        userScriptList: action.payload
      };

    default:
      return state;
  }
};
