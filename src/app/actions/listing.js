import types from "../types";

export const addRelatedScripts = data => {
  return {
    type: types.SET_RELATED_SCRIPT,
    payload: data
  };
};

export const updateDetail = data => {
  return {
    type: types.GET_SCRIPT_DETAIL,
    payload: data
  };
};

export const toggleLoading = bool => {
  return {
    type: types.SET_LOADING,
    bool: bool
  };
};

export const getTotalPages = data => {
  return {
    type: types.SET_PAGES,
    number: data
  };
};
export const getCategories = data => {
  return {
    type: types.GET_CATEGORIES,
    payload: data
  };
};
export const setSearchData = data => {
  return {
    type: types.SET_SEARCH,
    payload: data
  };
};
