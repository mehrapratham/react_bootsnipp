import * as auth from "./auth";
import * as auth from "./listing";

export default {
  ...auth,
  ...listing
};
