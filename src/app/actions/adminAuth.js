import types from "../types";

export const saveAdminScripts = data => {
  return {
    type: types.SAVE_ADMIN_SCRIPTS,
    payload: data
  };
};

export const saveNewScripts = data => {
  return {
    type: types.SAVE_NEW_SCRIPTS,
    payload: data
  };
};

export const saveCategory = data => {
  return {
    type: types.SAVE_CATEGORY,
    payload: data
  };
};

export const saveNewCategory = data => {
  return {
    type: types.SAVE_NEW_CATEGORY,
    payload: data
  };
};

export const updateCategory = data => {
  return {
    type: types.UPDATE_CATEGORY,
    payload: data
  };
};

export const updateScript = data => {
  return {
    type: types.UPDATE_SCRIPT,
    payload: data
  };
};
