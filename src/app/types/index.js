import * as listing from "./listing";
import * as adminAuth from "./adminAuth";
import * as auth from "./auth";

export default {
  ...listing,
  ...adminAuth,
  ...auth
};
