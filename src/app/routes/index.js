import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import AuthenticatedRoute from "../components/authenticated-route";
import UnauthenticatedRoute from "../components/unauthenticated-route";
import Loadable from "react-loadable";

import NotFound from "./not-found";

const Homepage = Loadable({
  loader: () => import(/* webpackChunkName: "homepage" */ "./homepage"),
  loading: () => null,
  modules: ["homepage"]
});

const checkAuth = () => {
  const token = localStorage.getItem("Admin_token");
  return !!token;
};

const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      checkAuth() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/admin-login" }} />
      )
    }
  />
);

const checkUserAuth = () => {
  const token = localStorage.getItem("User_token");
  return !!token;
};

const UserAuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      checkUserAuth() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: "/" }} />
      )
    }
  />
);

export default () => (
  <Switch>
    <Route exact path="/" component={Homepage} />
    {/* <Route exact path="/search" component={Homepage} />
    <Route exact path="/detailPage/:id" component={DetailPage} />

    <UserAuthRoute exact path="/myscripts" component={Homepage} />

    <Route exact path="/contact" component={Contact} />
    <Route exact path="/privacy" component={Privacy} />
    <Route exact path="/term" component={Term} />

    <Route exact path="/verifyaccount/:token" component={VerifyAccount} />

    
    <Route exact path="/admin-login" component={Admin} />
    <AuthRoute path="/admin" component={props => <Main {...props} />} />

    <Route component={NotFound} /> */}
  </Switch>
);
